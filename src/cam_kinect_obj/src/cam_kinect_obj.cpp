    #include <ros/ros.h>
    #include <image_transport/image_transport.h>
    #include <opencv2/highgui/highgui.hpp>
    #include <visualization_msgs/MarkerArray.h>
    #include <object_recognition_msgs/RecognizedObjectArray.h>
    #include <sensor_msgs/PointCloud2.h>
    #include <sensor_msgs/image_encodings.h>
    #include <geometry_msgs/Point.h>
    #include <image_geometry/pinhole_camera_model.h>
    #include <tf/transform_listener.h>
    #include <tf_conversions/tf_eigen.h>
    #include <cv_bridge/cv_bridge.h>
    #include <boost/foreach.hpp>
    #include <pcl/point_types.h>
    //#include <pcl/PCLPointCloud2.h>
    //#include <pcl/conversions.h>
    //#include <pcl_ros/impl/transfoms.hpp>
    #include <pcl/visualization/cloud_viewer.h>
    #include <pcl/filters/filter.h>
    #include <pcl_ros/point_cloud.h>
    #include <pcl_ros/transforms.h>
    #include <pcl_conversions/pcl_conversions.h>
    #include <Eigen/Core>
    #include <Eigen/Geometry>
    #include <string>
    #include <jsoncpp/json.h>
    #include <curl/curl.h>


bool pclActivated = false;
bool markerActivated = false;
bool objRecActivated = true;

int R = 0;
int G = 0;
int B = 175;

tf::StampedTransform transform;
    //pcl::visualization::CloudViewer viewer ("Cloud Viewer");
image_transport::CameraSubscriber sub_leftImage;
image_transport::Publisher pub_leftImage;
image_transport::CameraSubscriber sub_rightImage;
image_transport::Publisher pub_rightImage;
ros::Subscriber sub_Depth;
ros::Subscriber sub_markerArray;
ros::Subscriber sub_recognizedObjects;
std_msgs::Header headerPCL;

cv::Mat last_l_image;
cv::Mat last_r_image;
    //ros::Time image_time;
image_geometry::PinholeCameraModel cam_l_model;
image_geometry::PinholeCameraModel cam_r_model;
image_geometry::PinholeCameraModel cam_model;
bool new_l_image;
bool new_r_image;


#define URL_SIZE 256
#define BUF_SIZE 10*1024

long bytesWritten = 0;
long bytesRead = 0;

static int writer(void* buf, size_t len, size_t size, void* userdata) {
  size_t sLen = len * size;

    // if this is zero, then it's done
    // we don't do any special processing on the end of the stream
  if (sLen > 0) {
        // >= to account for terminating null
    if (bytesWritten + sLen >= BUF_SIZE) {
      return 0;
    }

    memcpy(&((char*)userdata)[bytesWritten], buf, sLen);
    bytesWritten += sLen;
  }

  return sLen;
}

std::string downloadJson(std::string URL)
{   
  CURL *curl;
  char tmp[BUF_SIZE];
  CURLcode res;
        struct curl_slist *headers=NULL; // init to NULL is important 
        std::ostringstream oss;
        curl_slist_append(headers, "Accept: application/json");  
        curl_slist_append(headers, "Content-Type: application/json");
        curl_slist_append(headers, "charsets: utf-8"); 
        curl = curl_easy_init();

        bytesWritten = 0;
        bytesRead = 0;

        // we'll use data to store the result
        memset(tmp, 0, BUF_SIZE);

        if (curl) 
        {
          curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
          curl_easy_setopt(curl, CURLOPT_URL, URL.c_str());
          curl_easy_setopt(curl, CURLOPT_HTTPGET,1); 
          curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers); 
          curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,writer);
          curl_easy_setopt(curl, CURLOPT_WRITEDATA, tmp);
          curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
          curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
          curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
          res = curl_easy_perform(curl);
          curl_easy_cleanup(curl);

            // copy the response to data
            //memcpy(data, tmp, BUF_SIZE);
            /*if (CURLE_OK == res) 
            { 
                char *ct;         
                res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct);
                //if((CURLE_OK == res) && ct)
                    //return *DownloadedResponse;
            }*/
              }
              return tmp;
            }



std::string getObjname(std::string db, std::string key)
{
              std::string object_name = "";
              Json::Value root;
              Json::Reader reader;
              bool parsedSuccess = reader.parse(db, root,false);

              std::stringstream ss;
              std::string strJson;

              if(parsedSuccess)
              {

                const Json::Value url = root["root"];
                const Json::Value collection = root["collection"];
                ss << url.asString() << "/" << collection.asString() << "/" << key;

                strJson = ss.str();

                std::string downloadedJson = downloadJson(strJson);

                //cout << downloadedJson;

                parsedSuccess = reader.parse(downloadedJson, root,false);

                if(parsedSuccess)
                {

                  const Json::Value object = root["object_name"];
                  const Json::Value description = root["description"];
                  object_name = object.asString();

                  //cout << "\nobject_name: " << object_name;
                }



              }
              return object_name;
}

            void imageRightCallback(const sensor_msgs::ImageConstPtr& img, const sensor_msgs::CameraInfoConstPtr& info_msg)
            {
              cv_bridge::CvImagePtr input_bridge;

              cam_r_model.fromCameraInfo(info_msg);
              cam_model = cam_r_model;
  //          image_time = ros::Time(0);
              new_r_image = true;  

              try
              {

                input_bridge = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
                last_r_image = input_bridge->image;

              }
              catch (cv_bridge::Exception& e)
              {
                ROS_ERROR("Could not convert from '%s' to 'bgr8'.", img->encoding.c_str());
              }

            }

            void imageLeftCallback(const sensor_msgs::ImageConstPtr& img, const sensor_msgs::CameraInfoConstPtr& info_msg)
            {
              cv_bridge::CvImagePtr input_bridge;

              cam_l_model.fromCameraInfo(info_msg);
              cam_model = cam_l_model;

      //image_time = ros::Time::now();
      //image_time = info_msg->header.stamp;
    //        image_time = ros::Time(0);
              new_l_image = true;  

              try
              {

                input_bridge = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
                last_l_image = input_bridge->image;

        //cv::imshow("view", cv_bridge::toCvShare(img, "bgr8")->image);
        //cv::waitKey(30);
              }
              catch (cv_bridge::Exception& e)
              {
                ROS_ERROR("Could not convert from '%s' to 'bgr8'.", img->encoding.c_str());
              }


            }

            void depthCallback(const sensor_msgs::PointCloud2& input)
            {
              headerPCL = input.header;

              tf::TransformListener tf_listener_;

              if ( new_l_image || new_r_image)
              {
               try {
                ros::Duration timeout(10.0/30);
                tf_listener_.waitForTransform(cam_model.tfFrame(), input.header.frame_id, ros::Time(0), timeout);
                tf_listener_.lookupTransform(cam_model.tfFrame(), input.header.frame_id, ros::Time(0), transform);

              }
              catch (tf::TransformException& ex) {
                ROS_WARN("[draw_frames] TF exception:\n%s", ex.what());
                return;
              }
            }
            if (! new_l_image || !pclActivated)
            {
              return;
            }
            else
            {

              new_l_image = false;

              pcl::PointCloud<pcl::PointXYZ> camera_cloud;
              pcl::fromROSMsg(input, camera_cloud);
              pcl_ros::transformPointCloud(camera_cloud, camera_cloud, transform);

              ROS_INFO_ONCE("[Raw PointCloud] Using an image size of (%d, %d)", last_l_image.rows, last_l_image.cols);

              cv::Mat newimage(last_l_image.size(), CV_8UC3);
          //newimage = cv::Scalar(0);
              newimage = last_l_image;
              for (size_t i =0; i<camera_cloud.size(); i++) {
                if (isnan (camera_cloud.points[i].x) || isnan (camera_cloud.points[i].y) || isnan (camera_cloud.points[i].z))
                 continue;
               cv::Point3d pt_cv(camera_cloud.points[i].x, camera_cloud.points[i].y, camera_cloud.points[i].z);
               cv::Point2d uv = cam_l_model.project3dToPixel(pt_cv);

               int image_i = int(uv.y);
               int image_j = int(uv.x);

               if (image_i >= last_l_image.rows || image_i < 0 || image_j >= last_l_image.cols || image_j < 0 ) {
                continue;
              }

              uchar r, g, b;

                    //using the source image
              cv::Point3_<uchar>* p = last_l_image.ptr<cv::Point3_<uchar> >(image_i, image_j);
              b = B;
                    //g = p->y*0.9;
              g = G;
              r = R;


              cv::Point3_<uchar>* q = newimage.ptr<cv::Point3_<uchar> >(image_i, image_j);
              q->x = b;
              q->y = g;
              q->z = r;

            }

            std_msgs::Header newheader;
            newheader.frame_id = cam_l_model.tfFrame();
            newheader.stamp = ros::Time(0);
            cv_bridge::CvImage cvimage(newheader, "bgr8", newimage);
            pub_leftImage.publish(cvimage.toImageMsg());
          }




          if (! new_r_image || !pclActivated)
          {
            return;
          }
          else
          {

            new_r_image = false;

            pcl::PointCloud<pcl::PointXYZ> camera_cloud;
            pcl::fromROSMsg(input, camera_cloud);
            pcl_ros::transformPointCloud(camera_cloud, camera_cloud, transform);

            ROS_INFO_ONCE("[Raw PointCloud] Using an image size of (%d, %d)", last_r_image.rows, last_r_image.cols);

            cv::Mat newimage(last_r_image.size(), CV_8UC3);
          //newimage = cv::Scalar(0);
            newimage = last_r_image;
            for (size_t i =0; i<camera_cloud.size(); i++) {
              if (isnan (camera_cloud.points[i].x) || isnan (camera_cloud.points[i].y) || isnan (camera_cloud.points[i].z))
               continue;
             cv::Point3d pt_cv(camera_cloud.points[i].x, camera_cloud.points[i].y, camera_cloud.points[i].z);
             cv::Point2d uv = cam_r_model.project3dToPixel(pt_cv);

             int image_i = int(uv.y);
             int image_j = int(uv.x);

             if (image_i >= last_r_image.rows || image_i < 0 || image_j >= last_r_image.cols || image_j < 0 ) {
              continue;
            }

            uchar r, g, b;

                    //using the source image
            cv::Point3_<uchar>* p = last_r_image.ptr<cv::Point3_<uchar> >(image_i, image_j);
            b = B;
                    //g = p->y*0.9;
            g = G;
            r = R;


            cv::Point3_<uchar>* q = newimage.ptr<cv::Point3_<uchar> >(image_i, image_j);
            q->x = b;
            q->y = g;
            q->z = r;

          }

          std_msgs::Header newheader;
          newheader.frame_id = cam_r_model.tfFrame();
          newheader.stamp = ros::Time(0);
          cv_bridge::CvImage cvimage(newheader, "bgr8", newimage);
          pub_rightImage.publish(cvimage.toImageMsg());
        }



    //    cv::imshow("view", newimage);
    //    cv::waitKey(30);
      //pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(&camera_cloud);

      //std::vector<int> indices;
      //pcl::removeNaNFromPointCloud(*cloud,*cloud, indices);

      //printf ("Cloud: width = %d, height = %d\n", cloud->width, cloud->height);

      //viewer.showCloud (cloud);

      }


      void markerArrayCallback(const visualization_msgs::MarkerArray& input)
      {

        if ( new_l_image && markerActivated)
        {

          new_l_image = false;
          
          ROS_INFO_ONCE("[MarkersArray] Using an image size of (%d, %d)", last_l_image.rows, last_l_image.cols);

          cv::Mat newimage(last_l_image.size(), CV_8UC3);
          newimage = last_l_image;

          //cout << "LEFT: x:" << transform.getOrigin().x() << " y:" << transform.getOrigin().y() << " z:" << transform.getOrigin().z();

          for (size_t i =0; i<input.markers.size(); i++) {

            for (size_t j =0; j<input.markers[i].points.size(); j++) {
              //cout << input.markers[i].points[j].x;
              cv::Point3d pt_cv(input.markers[i].points[j].x+transform.getOrigin().x(), input.markers[i].points[j].y+transform.getOrigin().y(), input.markers[i].points[j].z+transform.getOrigin().z());
              cv::Point2d uv = cam_l_model.project3dToPixel(pt_cv);

              int image_i = int(uv.y);
              int image_j = int(uv.x);

              if (image_i >= last_l_image.rows || image_i < 0 || image_j >= last_l_image.cols || image_j < 0 ) {
                continue;
              }

              uchar r, g, b;

                        //using the source image
              cv::Point3_<uchar>* p = last_l_image.ptr<cv::Point3_<uchar> >(image_i, image_j);
                        //b = p->x*0.9;
              b = B;
              g = G;
              r = R;


              cv::Point3_<uchar>* q = newimage.ptr<cv::Point3_<uchar> >(image_i, image_j);
              q->x = b;
              q->y = g;
              q->z = r;
            }
          }

          std_msgs::Header newheader;
          newheader.frame_id = cam_l_model.tfFrame();
          newheader.stamp = ros::Time(0);
          cv_bridge::CvImage cvimage(newheader, "bgr8", newimage);
          pub_leftImage.publish(cvimage.toImageMsg());

        }

        if ( new_r_image && markerActivated)
        {

          new_r_image = false;

          ROS_INFO_ONCE("[MarkersArray] Using an image size of (%d, %d)", last_r_image.rows, last_r_image.cols);

          cv::Mat newimage(last_r_image.size(), CV_8UC3);
          //newimage = cv::Scalar(0);
          newimage = last_r_image;

          cout << "RIGHT: x:" << transform.getOrigin().x() << " y:" << transform.getOrigin().y() << " z:" << transform.getOrigin().z();

          for (size_t i =0; i<input.markers.size(); i++) {

            for (size_t j =0; j<input.markers[i].points.size(); j++) {
              //cout << input.markers[i].points[j].x;
              cv::Point3d pt_cv(input.markers[i].points[j].x+transform.getOrigin().x(), input.markers[i].points[j].y+transform.getOrigin().y(), input.markers[i].points[j].z+transform.getOrigin().z());
              cv::Point2d uv = cam_r_model.project3dToPixel(pt_cv);

              int image_i = int(uv.y);
              int image_j = int(uv.x);

              if (image_i >= last_r_image.rows || image_i < 0 || image_j >= last_r_image.cols || image_j < 0 ) {
                continue;
              }

              uchar r, g, b;

                        //using the source image
              cv::Point3_<uchar>* p = last_r_image.ptr<cv::Point3_<uchar> >(image_i, image_j);
                        //b = p->x*0.9;
              b = B;
              g = G;
              r = R;


              cv::Point3_<uchar>* q = newimage.ptr<cv::Point3_<uchar> >(image_i, image_j);
              q->x = b;
              q->y = g;
              q->z = r;
            }
          }

          std_msgs::Header newheader;
          newheader.frame_id = cam_r_model.tfFrame();
          newheader.stamp = ros::Time(0);
          cv_bridge::CvImage cvimage(newheader, "bgr8", newimage);
          pub_rightImage.publish(cvimage.toImageMsg());
        }
      }


      void recognizedCallback(const object_recognition_msgs::RecognizedObjectArray& objs)
      {



        if ( new_l_image )
        {
         cv::Mat newimage_l(last_l_image.size(), CV_8UC3);
         newimage_l = last_l_image;
         int objname_x = 0;
         int objname_y = 0;

         if ( objRecActivated)
         {
          for (size_t i =0; i<objs.objects.size(); i++) {

            std::string object_name = "";

            for (size_t j =0; j<objs.objects[i].point_clouds.size(); j++) {

              object_name = getObjname(objs.objects[i].type.db,objs.objects[i].type.key);
              //cout << object_name;

              sensor_msgs::PointCloud2 input = objs.objects[i].point_clouds[j];

              new_l_image = false;

              //cout << "LEFT: x:" << transform.getOrigin().x() << " y:" << transform.getOrigin().y() << " z:" << transform.getOrigin().z();
              pcl::PointCloud<pcl::PointXYZ> camera_cloud_l;
              pcl::fromROSMsg(input, camera_cloud_l);
  //            pcl_ros::transformPointCloud(camera_cloud_l, camera_cloud_l, transform);

              ROS_INFO_ONCE("[RecognizedObjects] Using an image size of (%d, %d)", last_l_image.rows, last_l_image.cols);

              for (size_t i =0; i<camera_cloud_l.size(); i++) {
                if (isnan (camera_cloud_l.points[i].x) || isnan (camera_cloud_l.points[i].y) || isnan (camera_cloud_l.points[i].z))
                 continue;
               cv::Point3d pt_cv_l(camera_cloud_l.points[i].x+transform.getOrigin().x(), camera_cloud_l.points[i].y+transform.getOrigin().y(), camera_cloud_l.points[i].z+transform.getOrigin().z());

  //	     cv::Point3d pt_cv_l(camera_cloud_l.points[i].x, camera_cloud_l.points[i].y, camera_cloud_l.points[i].z);
               cv::Point2d uv_l = cam_l_model.project3dToPixel(pt_cv_l);
               
                int image_i_l = int(uv_l.y);
                int image_j_l = int(uv_l.x);

                if(i == 0)
               {
                   objname_x = image_j_l-30;
                   objname_y = image_i_l;
                }
               
               if (image_i_l >= last_l_image.rows || image_i_l < 0 || image_j_l >= last_l_image.cols || image_j_l < 0 ) {
                continue;
              }

              uchar r_l, g_l, b_l;

                    //using the source image
              cv::Point3_<uchar>* p_l = last_l_image.ptr<cv::Point3_<uchar> >(image_i_l, image_j_l);
                    //b_l = p_l->x;
              g_l = G;
              r_l = R;
                    //r_l = 51;
                    //g_l = 204;
              b_l = B;


              cv::Point3_<uchar>* q_l = newimage_l.ptr<cv::Point3_<uchar> >(image_i_l, image_j_l);
              q_l->x = b_l;
              q_l->y = g_l;
              q_l->z = r_l;

            }
            cv::putText(newimage_l, object_name, cvPoint(objname_x,objname_y), 1, 1.2, cvScalar(B,G,R), 1, CV_AA);



          }
        }
      }

      std_msgs::Header newheader_l;
      newheader_l.frame_id = cam_l_model.tfFrame();
      newheader_l.stamp = ros::Time(0);
      cv_bridge::CvImage cvimage_l(newheader_l, "bgr8", newimage_l);
      pub_leftImage.publish(cvimage_l.toImageMsg());
    }




    if (new_r_image )
    {
      cv::Mat newimage_r(last_r_image.size(), CV_8UC3);
      newimage_r = last_r_image;
         int objname_x = 0;
         int objname_y = 0;

      if (objRecActivated)
      {
        for (size_t i =0; i<objs.objects.size(); i++) {

          std::string object_name = "";
          std::string object_desc = "";

          for (size_t j =0; j<objs.objects[i].point_clouds.size(); j++) {
            

            object_name = getObjname(objs.objects[i].type.db,objs.objects[i].type.key);
            //cout << object_name;

            //cout << objs.objects[i].point_clouds[j];
            sensor_msgs::PointCloud2 input = objs.objects[i].point_clouds[j];

            new_r_image = false;

            pcl::PointCloud<pcl::PointXYZ> camera_cloud_r;
            pcl::fromROSMsg(input, camera_cloud_r);
            //pcl_ros::transformPointCloud(camera_cloud_r, camera_cloud_r, transform);

  //        cout << "RIGHT: x:" << transform.getOrigin().x() << " y:" << transform.getOrigin().y() << " z:" << transform.getOrigin().z();
            ROS_INFO_ONCE("[RecognizedObjects] Using an image size of (%d, %d)", last_r_image.rows, last_r_image.cols);


            
            for (size_t i =0; i<camera_cloud_r.size(); i++) {
              if (isnan (camera_cloud_r.points[i].x) || isnan (camera_cloud_r.points[i].y) || isnan (camera_cloud_r.points[i].z))
               continue;
             //cv::Point3d pt_cv_r(camera_cloud_r.points[i].x, camera_cloud_r.points[i].y, camera_cloud_r.points[i].z);
             cv::Point3d pt_cv_r(camera_cloud_r.points[i].x+transform.getOrigin().x(), camera_cloud_r.points[i].y+transform.getOrigin().y(), camera_cloud_r.points[i].z+transform.getOrigin().z());
             cv::Point2d uv_r = cam_r_model.project3dToPixel(pt_cv_r);

             int image_i_r = int(uv_r.y);
             int image_j_r = int(uv_r.x);

                if(i == 0)
               {
                   objname_x = image_j_r-30;
                   objname_y = image_i_r;
                }

             if (image_i_r >= last_r_image.rows || image_i_r < 0 || image_j_r >= last_r_image.cols || image_j_r < 0 )
             {
              continue;
            }

            uchar r_r, g_r, b_r;

                    //using the source image
            cv::Point3_<uchar>* p_r = last_r_image.ptr<cv::Point3_<uchar> >(image_i_r, image_j_r);
                    //b_r = p_r->x;
            g_r = R;
            r_r = G;
                    //r_r = 51;
    		//g_r = 204;
            b_r = B;

            cv::Point3_<uchar>* q_r = newimage_r.ptr<cv::Point3_<uchar> >(image_i_r, image_j_r);
            q_r->x = b_r;
            q_r->y = g_r;
            q_r->z = r_r;

          }
            cv::putText(newimage_r, object_name, cvPoint(objname_x,objname_y), 1, 1.2, cvScalar(B,G,R), 1, CV_AA);

        }
      }


    }

    std_msgs::Header newheader_r;
    newheader_r.frame_id = cam_r_model.tfFrame();
    newheader_r.stamp = ros::Time(0);
    cv_bridge::CvImage cvimage_r(newheader_r, "bgr8", newimage_r);
    pub_rightImage.publish(cvimage_r.toImageMsg());
  }
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "image_listener");
      //cv::namedWindow("view");
      //cv::startWindowThread();
  ros::NodeHandle nh;

  image_transport::ImageTransport it(nh);
  sub_markerArray = nh.subscribe("tabletop/clusters", 1, markerArrayCallback);
  sub_recognizedObjects = nh.subscribe("recognized_object_array", 1, recognizedCallback);
  sub_Depth = nh.subscribe("head_mount_kinect/depth_registered/points", 1, depthCallback);
  sub_leftImage = it.subscribeCamera("wide_stereo/left/image_color", 1, imageLeftCallback);
  sub_rightImage = it.subscribeCamera("wide_stereo/right/image_color", 1, imageRightCallback);
      //sub_leftImage = it.subscribeCamera("head_mount_kinect/rgb/image_color", 1, imageLeftCallback);
  pub_leftImage = it.advertise("oculus/left/image", 1);
  pub_rightImage = it.advertise("oculus/right/image", 1);


  ros::Rate loop_rate(5);

      while (nh.ok() /*&& !viewer.wasStopped()*/) {

  ros::spinOnce();
  loop_rate.sleep();
}

    //cv::destroyWindow("view");
}

