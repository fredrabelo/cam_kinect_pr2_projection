    #include <ros/ros.h>
    #include <image_transport/image_transport.h>
    #include <opencv2/highgui/highgui.hpp>
    #include <visualization_msgs/MarkerArray.h>
    #include <object_recognition_msgs/RecognizedObjectArray.h>
    #include <sensor_msgs/PointCloud2.h>
    #include <sensor_msgs/image_encodings.h>
    #include <geometry_msgs/Point.h>
    #include <image_geometry/pinhole_camera_model.h>
    #include <tf/transform_listener.h>
    #include <tf_conversions/tf_eigen.h>
    #include <cv_bridge/cv_bridge.h>
    #include <boost/foreach.hpp>
    #include <pcl/point_types.h>
    #include <pcl/visualization/cloud_viewer.h>
    #include <pcl/filters/filter.h>
    #include <pcl_ros/point_cloud.h>
    #include <pcl_ros/transforms.h>
    #include <pcl_conversions/pcl_conversions.h>
    #include <Eigen/Core>
    #include <Eigen/Geometry>
    #include <string>

#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>


bool pclActivated = true;

int R = 0;
int G = 0;
int B = 175;

int r_colors[10] = {4,179,4,173,4,255,212,2,212,6};
int g_colors[10] = {24,4,179,179,179,255,2,212,96,158};
int b_colors[10] = {179,4,47,4,167,255,212,138,2,95};


tf::StampedTransform transform;
//pcl::visualization::CloudViewer viewer ("Cloud Viewer");
image_transport::CameraSubscriber sub_leftImage;
image_transport::Publisher pub_leftImage;
image_transport::CameraSubscriber sub_rightImage;
image_transport::Publisher pub_rightImage;
ros::Subscriber sub_Depth;
std_msgs::Header headerPCL;

cv::Mat last_l_image;
cv::Mat last_r_image;
image_geometry::PinholeCameraModel cam_l_model;
image_geometry::PinholeCameraModel cam_r_model;
image_geometry::PinholeCameraModel cam_model;
bool new_l_image;
bool new_r_image;

            void imageRightCallback(const sensor_msgs::ImageConstPtr& img, const sensor_msgs::CameraInfoConstPtr& info_msg)
            {
              cv_bridge::CvImagePtr input_bridge;

              cam_r_model.fromCameraInfo(info_msg);
              cam_model = cam_r_model;
              new_r_image = true;  

              try
              {

                input_bridge = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
                last_r_image = input_bridge->image;

              }
              catch (cv_bridge::Exception& e)
              {
                ROS_ERROR("Could not convert from '%s' to 'bgr8'.", img->encoding.c_str());
              }

            }

            void imageLeftCallback(const sensor_msgs::ImageConstPtr& img, const sensor_msgs::CameraInfoConstPtr& info_msg)
            {
              cv_bridge::CvImagePtr input_bridge;

              cam_l_model.fromCameraInfo(info_msg);
              cam_model = cam_l_model;

              new_l_image = true;  

              try
              {

                input_bridge = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
                last_l_image = input_bridge->image;

              }
              catch (cv_bridge::Exception& e)
              {
                ROS_ERROR("Could not convert from '%s' to 'bgr8'.", img->encoding.c_str());
              }


            }

            void depthCallback(const sensor_msgs::PointCloud2& input)
            {


              headerPCL = input.header;

              tf::TransformListener tf_listener_;

              if ( new_l_image || new_r_image)
              {
               try {
                ros::Duration timeout(10.0/30);
                tf_listener_.waitForTransform(cam_model.tfFrame(), input.header.frame_id, ros::Time(0), timeout);
                tf_listener_.lookupTransform(cam_model.tfFrame(), input.header.frame_id, ros::Time(0), transform);

              }
              catch (tf::TransformException& ex) {
                ROS_WARN("[draw_frames] TF exception:\n%s", ex.what());
                return;
              }
            }
            if (! new_l_image || !pclActivated)
            {
              return;
            }
            else
            {

              new_l_image = false;



















              pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
              pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_f(new pcl::PointCloud<pcl::PointXYZRGB>);
              pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cl(new pcl::PointCloud<pcl::PointXYZRGB>);
              pcl::fromROSMsg(input, *cloud);







// Create the filtering object: downsample the dataset using a leaf size of 1cm
  pcl::VoxelGrid<pcl::PointXYZRGB> vg;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
  vg.setInputCloud (cloud);
  vg.setLeafSize (0.01f, 0.01f, 0.01f);
  vg.filter (*cloud_filtered);
  //std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl;
  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation<pcl::PointXYZRGB> seg;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGB> ());
  pcl::PCDWriter writer;
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (1000);
  seg.setDistanceThreshold (0.01);

  int i=0, nr_points = (int) cloud_filtered->points.size ();
  while (cloud_filtered->points.size () > 0.3 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_filtered);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      //std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers);
    extract.setNegative (false);

    // Get the points associated with the planar surface
    extract.filter (*cloud_plane);
    //std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_f);
    *cloud_filtered = *cloud_f;
  }

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud (cloud_filtered);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
  ec.setClusterTolerance (0.015); // 2cm
  ec.setMinClusterSize (100);
  ec.setMaxClusterSize (25000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (cloud_filtered);
  ec.extract (cluster_indices);

  int j = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGB>);
    //int R = 255, G = 255, B = 255;
    int B = b_colors[(j%10)];
    int G = g_colors[(j%10)]; 
    int R = r_colors[(j%10)]; 
    if(j > 0)
    {
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
    {
      cloud_cluster->points.push_back (cloud_filtered->points[*pit]); 
      cloud_filtered->points[*pit].b = B;
      cloud_filtered->points[*pit].g = G;
      cloud_filtered->points[*pit].r = R;
      cloud_cl->points.push_back (cloud_filtered->points[*pit]);
    }
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;

    //std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
    }
    j++;
  }



              pcl::PointCloud<pcl::PointXYZRGB> camera_cloud(*cloud_cl);
              //pcl::fromROSMsg(input, camera_cloud);
























              pcl_ros::transformPointCloud(camera_cloud, camera_cloud, transform);

              ROS_INFO_ONCE("[Raw PointCloud] Using an image size of (%d, %d)", last_l_image.rows, last_l_image.cols);

              cv::Mat newimage(last_l_image.size(), CV_8UC3);
              newimage = last_l_image;
              for (size_t i =0; i<camera_cloud.size(); i++) {
                if (isnan (camera_cloud.points[i].x) || isnan (camera_cloud.points[i].y) || isnan (camera_cloud.points[i].z))
                 continue;
               cv::Point3d pt_cv(camera_cloud.points[i].x, camera_cloud.points[i].y, camera_cloud.points[i].z);
               cv::Point2d uv = cam_l_model.project3dToPixel(pt_cv);

               int image_i = int(uv.y);
               int image_j = int(uv.x);

               if (image_i >= last_l_image.rows || image_i < 0 || image_j >= last_l_image.cols || image_j < 0 ) {
                continue;
              }

              uchar r, g, b;

              b = camera_cloud.points[i].b;
              g = camera_cloud.points[i].g;
              r = camera_cloud.points[i].r;


              cv::Point3_<uchar>* q = newimage.ptr<cv::Point3_<uchar> >(image_i, image_j);
              q->x = b;
              q->y = g;
              q->z = r;

            }

            std_msgs::Header newheader;
            newheader.frame_id = cam_l_model.tfFrame();
            newheader.stamp = ros::Time(0);
            cv_bridge::CvImage cvimage(newheader, "bgr8", newimage);
            pub_leftImage.publish(cvimage.toImageMsg());
          }




          if (! new_r_image || !pclActivated)
          {
            return;
          }
          else
          {

            new_r_image = false;



















              pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
              pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_f(new pcl::PointCloud<pcl::PointXYZRGB>);
              pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cl(new pcl::PointCloud<pcl::PointXYZRGB>);
              pcl::fromROSMsg(input, *cloud);







// Create the filtering object: downsample the dataset using a leaf size of 1cm
  pcl::VoxelGrid<pcl::PointXYZRGB> vg;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
  vg.setInputCloud (cloud);
  vg.setLeafSize (0.01f, 0.01f, 0.01f);
  vg.filter (*cloud_filtered);
  //std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; 

  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation<pcl::PointXYZRGB> seg;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGB> ());
  pcl::PCDWriter writer;
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (1000);
  seg.setDistanceThreshold (0.01);

  int i=0, nr_points = (int) cloud_filtered->points.size ();
  while (cloud_filtered->points.size () > 0.3 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_filtered);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      //std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers);
    extract.setNegative (false);

    // Get the points associated with the planar surface
    extract.filter (*cloud_plane);
    //std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_f);
    *cloud_filtered = *cloud_f;
  }

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud (cloud_filtered);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
  ec.setClusterTolerance (0.015); // 2cm
  ec.setMinClusterSize (100);
  ec.setMaxClusterSize (25000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (cloud_filtered);
  ec.extract (cluster_indices);

  int j = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGB>);
    //int R = 255, G = 255, B = 255;
    int B = b_colors[(j%10)];
    int G = g_colors[(j%10)]; 
    int R = r_colors[(j%10)]; 
    if(j > 0)
    {
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
    {
      cloud_cluster->points.push_back (cloud_filtered->points[*pit]); 
      cloud_filtered->points[*pit].b = B;
      cloud_filtered->points[*pit].g = G;
      cloud_filtered->points[*pit].r = R;
      cloud_cl->points.push_back (cloud_filtered->points[*pit]);
    }
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;

    //std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
    }
    j++;
  }



              pcl::PointCloud<pcl::PointXYZRGB> camera_cloud(*cloud_cl);
              //pcl::fromROSMsg(input, camera_cloud);



























            pcl_ros::transformPointCloud(camera_cloud, camera_cloud, transform);

            ROS_INFO_ONCE("[Raw PointCloud] Using an image size of (%d, %d)", last_r_image.rows, last_r_image.cols);

            cv::Mat newimage(last_r_image.size(), CV_8UC3);

            newimage = last_r_image;
            for (size_t i =0; i<camera_cloud.size(); i++) {
              if (isnan (camera_cloud.points[i].x) || isnan (camera_cloud.points[i].y) || isnan (camera_cloud.points[i].z))
               continue;
             cv::Point3d pt_cv(camera_cloud.points[i].x, camera_cloud.points[i].y, camera_cloud.points[i].z);
             cv::Point2d uv = cam_r_model.project3dToPixel(pt_cv);

             int image_i = int(uv.y);
             int image_j = int(uv.x);

             if (image_i >= last_r_image.rows || image_i < 0 || image_j >= last_r_image.cols || image_j < 0 ) {
              continue;
            }

            uchar r, g, b;

              b = camera_cloud.points[i].b;
              g = camera_cloud.points[i].g;
              r = camera_cloud.points[i].r;


            cv::Point3_<uchar>* q = newimage.ptr<cv::Point3_<uchar> >(image_i, image_j);
            q->x = b;
            q->y = g;
            q->z = r;

          }

          std_msgs::Header newheader;
          newheader.frame_id = cam_r_model.tfFrame();
          newheader.stamp = ros::Time(0);
          cv_bridge::CvImage cvimage(newheader, "bgr8", newimage);
          pub_rightImage.publish(cvimage.toImageMsg());
        }



    //    cv::imshow("view", newimage);
    //    cv::waitKey(30);
      //pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(&camera_cloud);

      //std::vector<int> indices;
      //pcl::removeNaNFromPointCloud(*cloud,*cloud, indices);

      //printf ("Cloud: width = %d, height = %d\n", cloud->width, cloud->height);

      //viewer.showCloud (cloud);

      }



int main(int argc, char **argv)
{
  ros::init(argc, argv, "image_listener");
      //cv::namedWindow("view");
      //cv::startWindowThread();
  ros::NodeHandle nh;

  image_transport::ImageTransport it(nh);
  sub_Depth = nh.subscribe("head_mount_kinect/depth_registered/points", 1, depthCallback);
  sub_leftImage = it.subscribeCamera("wide_stereo/left/image_color", 1, imageLeftCallback);
  sub_rightImage = it.subscribeCamera("wide_stereo/right/image_color", 1, imageRightCallback);
  pub_leftImage = it.advertise("oculus/left/image", 1);
  pub_rightImage = it.advertise("oculus/right/image", 1);


  ros::Rate loop_rate(5);

      while (nh.ok() /*&& !viewer.wasStopped()*/) {

  ros::spinOnce();
  loop_rate.sleep();
}

    //cv::destroyWindow("view");
}

